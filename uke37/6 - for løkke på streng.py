'''for-løkker på streng'''

tekst = 'Uten mat og drikke, duger helten ikke'
vokaler = 'aeiouyæøå'

# print ut alle vokaler i tekst-variabelen
# for bokstav in tekst:
#     if (bokstav.lower() in vokaler):
#         print(bokstav)


# print ut alle konsonanter i tekst-variablenen
#(det er lov å også printe ut mellomrom og komma)
for letter in tekst:
    if (letter.lower() not in vokaler):
        print(letter)

