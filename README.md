# ITGK 2021 magnustvilde forelesningskode
Velkommen til mappestruktur for forelesningskoden til Magnus Tvilde i ITGK høsten 2021!

Her blir koden som blir skrevet og lagret i forelesningen tilgjengeliggjort slik at du kan kopiere den, prøve den selv og endre på den.

## Parallell 2
Forelesningskode fra parallell 2 (torsdager 08:15-10:00, samt onsdag uke 34+35) høsten 2021. Filene finner du i riktig ukenummer her.

## Parallell 3 uke 34+35
Forelesningskode parallell 3 for de første ukene er også tilgjengelig her. Den blir publisert i riktig ukenummer, og så i 'parallell3'.

## Tilbakemelding
Gjerne kom med tilbakemelding på forelesningskoden, mappestruktur eller lignende på magnus.tvilde 'alfakrøll' ntnu.no.